package resolution.example6.zzeulki.practice132;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.SystemClock;
import android.widget.RemoteViews;

import java.util.HashMap;

/**
 * Implementation of App Widget functionality.
 */
public class NewAppWidget extends AppWidgetProvider {

    private static HashMap<Integer,Integer> mCountMap = new HashMap<Integer, Integer>();
    private static HashMap<Integer,PendingIntent> mActionMap;
    public static int mupdateinterval;

    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        mActionMap = new HashMap<Integer, PendingIntent>();

        CharSequence widgetText = context.getString(R.string.appwidget_text);
        // Construct the RemoteViews object
        RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.new_app_widget);

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);
    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        super.onUpdate(context,appWidgetManager,appWidgetIds);

        for(int appWidgetId : appWidgetIds){
            updateCount(context.getPackageName(), appWidgetManager,appWidgetId);

            PendingIntent action;
            action = mActionMap.get(appWidgetId);
            if(action==null){
                action = makeUpdateAction(context, MyService.UPDATE,appWidgetId);
                mActionMap.put(appWidgetId,action);
                setAlarm(context,mupdateinterval,action);
            }
        }

    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
        super.onEnabled(context);

        mActionMap = new HashMap<Integer, PendingIntent>();
        mupdateinterval = 1000;
    }

    @Override
    public void onDisabled(Context context) {
        super.onDisabled(context);
    }

    @Override
    public void onReceive(Context context, Intent intent){
        super.onReceive(context,intent);
    }



    public static void updateCount(String packageName, AppWidgetManager appWidgetManager, int appWidgetId){
        Integer count = mCountMap.get(appWidgetId);

        if(count==null)
            count = 0;

        String output;
        output = "Count : " + count;
        int resid;
        if((count%2)==0)
            resid = R.drawable.heart2;
        else
            resid = R.drawable.heart1;

        count++;

        mCountMap.put(appWidgetId,count);

        RemoteViews views = new RemoteViews(packageName, R.layout.new_app_widget);
        views.setImageViewResource(R.id.imageView1,resid);
        views.setTextViewText(R.id.textView1,output);

        appWidgetManager.updateAppWidget(appWidgetId,views);
    }

    public static PendingIntent makeUpdateAction(Context context, String command, int appWidgetId){
        Intent active = new Intent(context, MyService.class);
        active.setAction(command);
        active.putExtra(AppWidgetManager.EXTRA_APPWIDGET_ID, appWidgetId);
        Uri data = Uri.withAppendedPath(Uri.parse("MyService://widget/id/#" + command + appWidgetId),String.valueOf(appWidgetId));
        active.setData(data);

        PendingIntent action = PendingIntent.getService(context, 0 , active, PendingIntent.FLAG_UPDATE_CURRENT);

        return action;
    }

    public static void setAlarm(Context context, int UpdateInterval, PendingIntent action){
        AlarmManager alarmManager = (AlarmManager) context.getSystemService(context.ALARM_SERVICE);

        if(UpdateInterval > 0){
            alarmManager.setRepeating(AlarmManager.ELAPSED_REALTIME, SystemClock.elapsedRealtime(),UpdateInterval,action);
        }else{
            alarmManager.cancel(action);
        }
    }
}


