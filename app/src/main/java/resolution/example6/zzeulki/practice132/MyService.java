package resolution.example6.zzeulki.practice132;

import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

public class MyService extends Service {
    public static final String UPDATE = "Update Myappwidget";

    @Override
    public void onStart(Intent intent, int startId){
        String command = intent.getAction();

        int appWidgetId = intent.getExtras().getInt(AppWidgetManager.EXTRA_APPWIDGET_ID);
        AppWidgetManager appWidgetManager = AppWidgetManager.getInstance(getApplicationContext());

        if(command.equals(UPDATE)){
            Log.i("kkk","ttt");
            NewAppWidget.updateCount(getApplicationContext().getPackageName(), appWidgetManager, appWidgetId);
        }

        super.onStart(intent, startId);
    }

    public MyService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        return null;
    }
}
